import { createStore } from "redux";
const botdata = {
   bots: [
      {
         "id": 1,
         "bot": "Hot Bot",
         "description": "Hot bot is low volatility portfolio of growth stocks selected based on our proprietary metrics",
         "index-value": 289.34,
         "cagr": 34
      },
      {
         "id": 2,
         "bot": "Cool Bot",
         "description": "Cool bot is low volatility portfolio of growth stocks selected based on our proprietary metrics",
         "index-value": 439.34,
         "cagr": 28
      },
      {
         "id": 3,
         "bot": "Options Bot",
         "description": "Options bot is low volatility portfolio of growth stocks selected based on our proprietary metrics",
         "index-value": 139.34,
         "cagr": 42
      }
   ],
   cartValue: 0

}
const dataReducer = (state = botdata, action) => {
   if (action.type == "addToCart") {
      return {
         ...state,
         cartValue: state.cartValue + 1
      }
   }
   return state;
}
const store = createStore(dataReducer)
export default store;