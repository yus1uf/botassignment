import Header from './component//Header'
import React from 'react';
import Dashboard from './component/Dashboard'
import { Route } from 'react-router';
import BotDetails from './component/BotDetails'
import './App.css';

function App() {

  return (
    <div className="App">
      <Header />
      <Route path='/bots'><Dashboard /></Route>
      <Route path={"/bots-detail/:id"}><BotDetails /></Route>
    </div>
  );
}

export default App;
