import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useParams } from 'react-router'
import './botdetails.css'

function BotDetails(props) {
    const param = useParams()

    const data = useSelector((state) => state.bots);

    const dispatch = useDispatch();

    const cartHandler = () => {
        dispatch({
            type: "addToCart"
        })
    }
    return (

        <div className="main">

            {
                data.filter((algo) => {
                    if (algo.id.toString() === param.id) {
                        return algo;
                    }
                }).map((algo) => {
                    return <>
                        <div className="main"></div>
                        <div className="bot-description">
                            <h6>{algo.bot}</h6>
                            <p>{algo.description}</p>
                        </div>
                        <div className="index">
                            <p>Index Value</p>
                            <p>{algo['index-value']}</p>
                        </div>
                        <div className="agcr">
                            <p>CAGR</p>
                            <p>{algo.cagr}</p>
                        </div>

                        <div className="btn">
                            <button>Moderate Risk</button>
                            <button onClick={cartHandler}>Buy</button></div>
                    </>
                })
            }
        </div>
    )
}
export default BotDetails;