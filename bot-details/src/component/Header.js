import React from "react";
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'

function Header() {
    const cart = useSelector((state) => {
        return state.cartValue
    });
    return (

        <div className='header'>
            <div>
                <Link to='/bots'> <h5>Dashboard</h5></Link>
            </div>
            <div className='cart'>
                <h5>Cart-{cart}</h5>
            </div>
        </div>
    )
}
export default Header;