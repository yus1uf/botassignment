import { Table, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'


function Dashboard(props) {
    const data = useSelector((state) => state.bots);
    const dispatch = useDispatch();
    const cartValueHandler = () => {
        dispatch({
            type: "addToCart"
        })
    }

    return (
        <div className='dashboard'>
            <p>Best algo suited for your need</p>
            {
                data.map((item, i) =>
                    <div className='items'>
                        <Table className='table' variant='dark' size='sm' >
                            <tbody>
                                <tr key={i}>
                                    <td>{item.bot}</td>
                                    <td>
                                        <table>
                                            <tbody>
                                                <tr key={i}>
                                                    <td>Index Value</td>
                                                    <td>AGCR</td>
                                                </tr>
                                                <tr align="center">
                                                    <td>{item['index-value']}</td>
                                                    <td>{item.cagr}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <Link to={`/bots-detail/${item.id}`}>
                                            <Button >View Algo</Button>
                                        </Link>
                                        <Button onClick={cartValueHandler}>Buy</Button>
                                    </td>
                                </tr>
                            </tbody>
                        </Table>
                    </div>
                )}
        </div>
    )
}
export default Dashboard;